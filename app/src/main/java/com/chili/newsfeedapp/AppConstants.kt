package com.chili.newsfeedapp

const val UPDATE_SAVED_LIST_EVENT = "update_saved_list_event"
const val NEWS_ITEM_URL = "news_item_url"

enum class Sources(val string: String){
    ABC_NEWS("abc-news"),
    ANSA("al-jazzera-english"),
    FOX_SPORTS("fox-sport"),


}