package com.chili.newsfeedapp

import android.app.Application
import com.chili.newsfeedapp.data.db.NewsDataSourceLocal
import com.chili.newsfeedapp.data.db.AppDatabase
import com.chili.newsfeedapp.data.network.NEWS_API_KEY
import com.chili.newsfeedapp.data.network.NewsApiService
import com.chili.newsfeedapp.data.network.NewsDataSourceRemote
import com.chili.newsfeedapp.data.repository.NewsRepository
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class NewsFeedApplication : Application() {
    companion object {
        lateinit var instance: NewsFeedApplication
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    val retrofitClient: Retrofit by lazy {
        val apiKeyInterceptor = Interceptor { it: Interceptor.Chain ->
            val url = it.request().url()
                    .newBuilder()
                    .addQueryParameter("apiKey", NEWS_API_KEY)
                    .build()

            val request = it.request().newBuilder().url(url).build()
            return@Interceptor it.proceed(request)
        }

        val okHttpClient = OkHttpClient
                .Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(apiKeyInterceptor)
                .build()


        Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://newsapi.org/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    val newsApiService: NewsApiService by lazy {
        retrofitClient.create(NewsApiService::class.java)
    }

    val appDatabase: AppDatabase by lazy {
        AppDatabase(this)
    }

    val newsFeedRepository: NewsRepository by lazy {
        NewsRepository(NewsDataSourceLocal(appDatabase.newsDao()), NewsDataSourceRemote(newsApiService))
    }


}
