package com.chili.newsfeedapp.adapters

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.chili.newsfeedapp.R

class FeedFragmentPagerAdapter(context: Context, fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager) {

    private var fragmentsTitles = context.resources.getStringArray(R.array.fragments_titles)
    private var fragments: HashSet<Fragment> = hashSetOf()


    override fun getItem(position: Int): Fragment {
        return fragments.elementAt(position)
    }

    override fun getCount(): Int {
        return fragments.size
    }

    // Returns the page title for the top indicator
    override fun getPageTitle(position: Int): CharSequence? {
        return fragmentsTitles[position]
    }

    fun setFragmentsList(fragments: HashSet<Fragment>) {
        this.fragments = fragments
    }


}