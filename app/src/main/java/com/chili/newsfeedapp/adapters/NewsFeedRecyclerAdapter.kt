package com.chili.newsfeedapp.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.chili.newsfeedapp.R
import com.chili.newsfeedapp.data.db.entity.NewsEntry
import com.chili.newsfeedapp.glide.GlideApp

class NewsFeedRecyclerAdapter(private var newsList: List<NewsEntry>) :
    RecyclerView.Adapter<NewsFeedRecyclerAdapter.NewsItemVH>() {

    private var itemClickListener: ItemClickListener? = null

    override fun onCreateViewHolder(viewGroup: ViewGroup, itemType: Int): NewsItemVH {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_news_feed, null)
        return NewsItemVH(v)
    }

    override fun getItemCount(): Int {
        return newsList.size
    }

    override fun onBindViewHolder(viewHolder: NewsItemVH, position: Int) {
        val item = getItem(position)
        viewHolder.deleteImage.visibility = View.GONE

        if (!item.urlToImage.isNullOrEmpty()) {
            GlideApp
                .with(viewHolder.itemView)
                .load(item.urlToImage)
                .centerCrop()
                .into(viewHolder.image)
        } else {
            viewHolder.image.setImageResource(R.drawable.ic_news_holder)
        }

        if (!item.title.isNullOrEmpty()) {
            viewHolder.title.text = item.title
        } else {
            viewHolder.title.visibility = View.GONE
        }

        if (!item.description.isNullOrEmpty()) {
            viewHolder.description.text = item.description
        } else {
            viewHolder.description.visibility = View.GONE
        }

        viewHolder.itemView.setOnClickListener {
            itemClickListener?.onItemClicked(item, position)
        }

    }

    fun setOnclickListener(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    private fun getItem(position: Int): NewsEntry {
        return newsList[position]
    }

    fun updateList(newsListUpdated: List<NewsEntry>) {
        newsList = newsListUpdated
        notifyDataSetChanged()
    }

    class NewsItemVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView = itemView.findViewById(R.id.image)
        var title: TextView = itemView.findViewById(R.id.title)
        var description: TextView = itemView.findViewById(R.id.description)
        var deleteImage: ImageView = itemView.findViewById(R.id.deleteImage)
    }

    interface ItemClickListener {
        fun onItemClicked(newsEntry: NewsEntry, position: Int)
    }
}