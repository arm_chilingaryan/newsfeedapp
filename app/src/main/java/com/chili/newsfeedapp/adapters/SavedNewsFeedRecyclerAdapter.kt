package com.chili.newsfeedapp.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.chili.newsfeedapp.R
import com.chili.newsfeedapp.data.db.entity.SavedNewsEntry
import com.chili.newsfeedapp.glide.GlideApp

class SavedNewsFeedRecyclerAdapter(private var savedNewsList: List<SavedNewsEntry>) :
        RecyclerView.Adapter<SavedNewsFeedRecyclerAdapter.SavedNewsItemVH>() {

    private var itemClickListener: ItemClickListener? = null

    override fun onCreateViewHolder(viewGroup: ViewGroup, itemType: Int): SavedNewsItemVH {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_news_feed, null)
        return SavedNewsItemVH(v)
    }

    override fun getItemCount(): Int {
        return savedNewsList.size
    }

    override fun onBindViewHolder(viewHolder: SavedNewsItemVH, position: Int) {
        val item = getItem(position)
        viewHolder.deleteImage.visibility = View.VISIBLE

        if (!item.urlToImage.isNullOrEmpty()) {
            GlideApp
                    .with(viewHolder.itemView)
                    .load(item.urlToImage)
                    .centerCrop()
                    .into(viewHolder.image)
        } else {
            viewHolder.image.setImageResource(R.drawable.ic_news_holder)
        }

        if (!item.title.isNullOrEmpty()) {
            viewHolder.title.text = item.title
        } else {
            viewHolder.title.visibility = View.GONE
        }

        if (!item.description.isNullOrEmpty()) {
            viewHolder.description.text = item.description
        } else {
            viewHolder.description.visibility = View.GONE
        }

        viewHolder.itemView.setOnClickListener {
            itemClickListener?.onNewsItemClicked(item, position)
        }

        viewHolder.deleteImage.setOnClickListener {
            itemClickListener?.onDeleteClicked(item)
        }

    }


    fun setOnclickListener(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    private fun getItem(position: Int): SavedNewsEntry {
        return savedNewsList[position]
    }

    fun updateList(savedNewsListUpdated: List<SavedNewsEntry>) {
        savedNewsList = savedNewsListUpdated
        notifyDataSetChanged()
    }

    class SavedNewsItemVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView = itemView.findViewById(R.id.image)
        var title: TextView = itemView.findViewById(R.id.title)
        var description: TextView = itemView.findViewById(R.id.description)
        var deleteImage: ImageView = itemView.findViewById(R.id.deleteImage)
    }

    interface ItemClickListener {
        fun onNewsItemClicked(newsEntry: SavedNewsEntry, position: Int)
        fun onDeleteClicked(newsEntry: SavedNewsEntry)
    }
}