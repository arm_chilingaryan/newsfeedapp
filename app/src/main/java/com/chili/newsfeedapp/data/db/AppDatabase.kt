package com.chili.newsfeedapp.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.chili.newsfeedapp.data.db.daos.NewsDao
import com.chili.newsfeedapp.data.db.daos.SavedNewsDao
import com.chili.newsfeedapp.data.db.entity.NewsEntry
import com.chili.newsfeedapp.data.db.entity.SavedNewsEntry

@Database(entities = [NewsEntry::class, SavedNewsEntry::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun newsDao(): NewsDao
    abstract fun savedNewsDao(): SavedNewsDao


    companion object {
        @Volatile
        private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "news.db")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build()
    }
}