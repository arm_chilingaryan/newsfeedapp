package com.chili.newsfeedapp.data.db

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.chili.newsfeedapp.data.db.daos.NewsDao
import com.chili.newsfeedapp.data.db.entity.NewsEntry
import com.chili.newsfeedapp.data.network.response.BaseNewsResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class NewsDataSourceLocal(private val newsDataSourceLocal: NewsDao) {

    private val _dbNews = MutableLiveData<List<NewsEntry>>()

    val dbNews: LiveData<List<NewsEntry>>
        get() = _dbNews


    @SuppressLint("CheckResult")
    fun storeIntoDb(newsResponse: BaseNewsResponse) {
        Observable.fromCallable {
            newsDataSourceLocal.insertAllNews(newsResponse.articles)
        }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe({
                    _dbNews.postValue(newsResponse.articles)
                }, {
                    Log.e("Insert", "Exception", it)

                })

    }

    @SuppressLint("CheckResult")
    fun getNews() {
        newsDataSourceLocal.getNews()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _dbNews.postValue(it)
                }, {
                    it.printStackTrace()

                })
    }
}