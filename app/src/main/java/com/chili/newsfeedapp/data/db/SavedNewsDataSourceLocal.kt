package com.chili.newsfeedapp.data.db

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.chili.newsfeedapp.data.db.daos.SavedNewsDao
import com.chili.newsfeedapp.data.db.entity.SavedNewsEntry
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SavedNewsDataSourceLocal(val savedNewsDao: SavedNewsDao) {

    private val _dbNews = MutableLiveData<List<SavedNewsEntry>>()

    val dbNews: LiveData<List<SavedNewsEntry>>
        get() = _dbNews

    @SuppressLint("CheckResult")
    fun getNews() {
        savedNewsDao.getSavedNews()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _dbNews.postValue(it)
                }, {
                    it.printStackTrace()

                })
    }
}