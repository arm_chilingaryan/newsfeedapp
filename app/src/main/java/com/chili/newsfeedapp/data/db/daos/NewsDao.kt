package com.chili.newsfeedapp.data.db.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.chili.newsfeedapp.data.db.entity.NewsEntry
import io.reactivex.Single

@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(newsEntry: NewsEntry)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllNews(newsList: List<NewsEntry>)

    @Query("select * from news")
    fun getNews(): Single<List<NewsEntry>>

    @Query("select * from news where url = :url")
    fun getNewsByUrl(url: String): NewsEntry
}