package com.chili.newsfeedapp.data.db.daos

import android.arch.persistence.room.*
import com.chili.newsfeedapp.data.db.entity.BaseNewsEntry
import com.chili.newsfeedapp.data.db.entity.NewsEntry
import com.chili.newsfeedapp.data.db.entity.SavedNewsEntry
import io.reactivex.Single

@Dao
interface SavedNewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(newsEntry: SavedNewsEntry)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllNews(newsList: List<SavedNewsEntry>)

    @Query("select * from saved_news order by publishedAt DESC")
    fun getSavedNews(): Single<List<SavedNewsEntry>>

    @Query("select * from saved_news where url = :url")
    fun getSavedNewsByUrl(url: String): SavedNewsEntry?

    @Query("delete from saved_news where url = :url")
    fun deleteSavedNewsByUrl(url: String)

    @Delete()
    fun deleteSavedNews(savedNewsEntry: SavedNewsEntry)
}