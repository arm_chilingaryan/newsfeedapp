package com.chili.newsfeedapp.data.db.entity

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

open class BaseNewsEntry(
        val publishedAt: String?,
        val urlToImage: String?,
        @Embedded(prefix = "source_")
        val source: Source?,
        val title: String?,
        @PrimaryKey
        @NonNull
        val url: String,
        val content: String?,
        val description: String?,
        var isSaved: Boolean = false
)


