package com.chili.newsfeedapp.data.db.entity

import android.arch.persistence.room.Entity

@Entity(tableName = "news")
class NewsEntry(
        publishedAt: String?,
        urlToImage: String?,
        source: Source?,
        title: String?,
        url: String,
        content: String?,
        description: String?,
        isSaved: Boolean = false
) : BaseNewsEntry(publishedAt, urlToImage, source, title, url, content,description, isSaved) {

    fun toSavedNewsEntry(): SavedNewsEntry {
        return SavedNewsEntry(publishedAt, urlToImage, source, title, url, content, description,isSaved)
    }
}