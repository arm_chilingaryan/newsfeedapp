package com.chili.newsfeedapp.data.db.entity

import android.arch.persistence.room.Entity

@Entity(tableName = "saved_news")

class SavedNewsEntry(
    publishedAt: String?,
    urlToImage: String?,
    source: Source?,
    title: String?,
    url: String,
    content: String?,
    description: String?,
    isSaved: Boolean = true
) : BaseNewsEntry(publishedAt, urlToImage, source, title, url, content, description, isSaved)