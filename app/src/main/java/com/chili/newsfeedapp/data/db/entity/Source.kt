package com.chili.newsfeedapp.data.db.entity

data class Source(
	val name: String?,
	val id: String?
)
