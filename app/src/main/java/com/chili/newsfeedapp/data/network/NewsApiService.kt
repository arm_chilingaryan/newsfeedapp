package com.chili.newsfeedapp.data.network

import com.chili.newsfeedapp.NewsFeedApplication
import com.chili.newsfeedapp.data.network.response.BaseNewsResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

const val NEWS_API_KEY = "a2c26bf1980b424884711f99deb315bc"

interface NewsApiService {

    @GET("everything")
    fun getNews(@Query("sources") sources: String): Observable<BaseNewsResponse>
}