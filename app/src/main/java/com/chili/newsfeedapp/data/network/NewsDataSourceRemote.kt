package com.chili.newsfeedapp.data.network

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.chili.newsfeedapp.Sources
import com.chili.newsfeedapp.data.network.response.BaseNewsResponse
import com.chili.newsfeedapp.extentions.showToast
import com.chili.newsfeedapp.extentions.showToastLong
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class NewsDataSourceRemote(private val newsApiService: NewsApiService) {

    private val _downloadedNews = MutableLiveData<BaseNewsResponse>()

    val downloadedNews: LiveData<BaseNewsResponse>
        get() = _downloadedNews

    @SuppressLint("CheckResult")
    fun getNews(newsSources: Array<Sources>) {
        newsApiService.getNews(newsSources.joinToString())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    _downloadedNews.postValue(it)
                },
                {
                    it.printStackTrace()
                    showToastLong("Check network connection please")
                })
    }
}