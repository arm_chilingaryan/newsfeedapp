package com.chili.newsfeedapp.data.network.response

import com.chili.newsfeedapp.data.db.entity.NewsEntry

data class BaseNewsResponse(
    val totalResults: Int,
    val articles: List<NewsEntry>,
    val status: String
)
