package com.chili.newsfeedapp.data.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.chili.newsfeedapp.Sources
import com.chili.newsfeedapp.data.db.NewsDataSourceLocal
import com.chili.newsfeedapp.data.db.entity.NewsEntry
import com.chili.newsfeedapp.data.network.NewsDataSourceRemote

class NewsRepository(
        private val newsDataSourceLocal: NewsDataSourceLocal,
        private val newsDataSourceRemote: NewsDataSourceRemote
) {
    var data = MutableLiveData<List<NewsEntry>>()

    fun getNewsFeed(newsSources : Array <Sources>): LiveData<List<NewsEntry>> {
        newsDataSourceRemote.getNews(newsSources)
        if (data.value == null) {
            newsDataSourceLocal.getNews()
        }
        return data
    }

    init {
        newsDataSourceRemote.downloadedNews.observeForever { updatedNews ->
            updatedNews?.let {
                newsDataSourceLocal.storeIntoDb(it)
            }
        }

        newsDataSourceLocal.dbNews.observeForever { updatedNews ->
            updatedNews?.let {
                data.value = it
            }
        }
    }


}