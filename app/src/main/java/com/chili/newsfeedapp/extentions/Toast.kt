package com.chili.newsfeedapp.extentions

import android.content.Context
import android.widget.Toast
import com.chili.newsfeedapp.NewsFeedApplication

fun showToast(string: String) {
    Toast.makeText(NewsFeedApplication.instance, string, Toast.LENGTH_SHORT).show()
}

fun showToastLong(string: String) {
    Toast.makeText(NewsFeedApplication.instance, string, Toast.LENGTH_LONG).show()
}

