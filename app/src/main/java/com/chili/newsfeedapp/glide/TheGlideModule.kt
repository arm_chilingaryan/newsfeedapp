package com.chili.newsfeedapp.glide

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class TheGlideModule : AppGlideModule()