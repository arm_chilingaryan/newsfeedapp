package com.chili.newsfeedapp.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.chili.newsfeedapp.R
import com.chili.newsfeedapp.adapters.FeedFragmentPagerAdapter
import com.chili.newsfeedapp.ui.newsfeed.NewsFeedFragment
import com.chili.newsfeedapp.ui.savedNews.SavedNewsFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initToolBar()
        initViewPager()
    }

    private fun initToolBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.app_name)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun initViewPager() {
        var fragments: HashSet<Fragment> = hashSetOf(NewsFeedFragment.newInstance(), SavedNewsFragment.newInstance())

        var feedFragmentPagerAdapter = FeedFragmentPagerAdapter(this, supportFragmentManager)
        feedFragmentPagerAdapter.setFragmentsList(fragments)

        pager.adapter = feedFragmentPagerAdapter
        tabs.setViewPager(pager)
    }

    override fun onBackPressed() {
        updateToolBar(supportFragmentManager.backStackEntryCount == 2)
        super.onBackPressed()
    }

    fun updateToolBar(showBackBtn: Boolean) {
        supportActionBar?.setDisplayHomeAsUpEnabled(showBackBtn)
        if (showBackBtn) {
            supportActionBar?.title = getString(R.string.details_fragment_title)
        } else {
            supportActionBar?.title = getString(R.string.app_name)
        }
    }


}

