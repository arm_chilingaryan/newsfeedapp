package com.chili.newsfeedapp.ui.details

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.chili.newsfeedapp.NEWS_ITEM_URL
import com.chili.newsfeedapp.NewsFeedApplication
import com.chili.newsfeedapp.R
import com.chili.newsfeedapp.data.db.entity.BaseNewsEntry
import com.chili.newsfeedapp.extentions.showToast
import com.chili.newsfeedapp.glide.GlideApp
import com.chili.newsfeedapp.ui.MainActivity
import kotlinx.android.synthetic.main.details_fragment.*

class DetailsFragment : Fragment() {

    companion object {
        fun newInstance() = DetailsFragment()
    }

    private lateinit var viewModel: DetailsViewModel
    private var newsUrl: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            newsUrl = it.getString(NEWS_ITEM_URL, "")
        }
        initFragmentTitle()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.details_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(
            this,
            DetailsViewModel.DetailsViewModelFactory(NewsFeedApplication.instance.appDatabase)
        ).get(DetailsViewModel::class.java)

        if (newsUrl.isNotEmpty()) {
            val baseNewsEntry = viewModel.getNewsByUrl(newsUrl)
            initUi(baseNewsEntry)
        } else {
            showToast("can`t find item")
            activity?.onBackPressed()
        }
    }


    private fun initFragmentTitle() {
        activity?.let {
            (it as MainActivity).apply {
                supportActionBar?.title = getString(R.string.details_fragment_title)
                updateToolBar(true)
            }
        }
    }

    private fun initUi(newsEntry: BaseNewsEntry) {

        if (!newsEntry.urlToImage.isNullOrEmpty()) {
            GlideApp
                .with(NewsFeedApplication.instance)
                .load(newsEntry.urlToImage)
                .centerCrop()
                .into(newsImage)
        } else {
            newsImage.scaleType = ImageView.ScaleType.CENTER_INSIDE
            newsImage.setImageResource(R.drawable.ic_news_holder)
        }

        if (!newsEntry.title.isNullOrEmpty()) {
            title.text = newsEntry.title
        } else {
            title.visibility = View.GONE
        }

        if (!newsEntry.description.isNullOrEmpty()) {
            description.text = newsEntry.description
        } else {
            description.visibility = View.GONE
        }

        if (!newsEntry.content.isNullOrEmpty()) {
            content.text = newsEntry.content
        } else {
            content.visibility = View.GONE
        }

        favoriteImg.isSelected = newsEntry.isSaved

        setonClickListeners()


    }

    private fun setonClickListeners() {
        favoriteImg.setOnClickListener {
            viewModel.insertOrDelete(it.isSelected, newsUrl)
            it.isSelected = !it.isSelected
        }
    }


}
