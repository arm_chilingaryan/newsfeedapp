package com.chili.newsfeedapp.ui.details

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import com.chili.newsfeedapp.NewsFeedApplication
import com.chili.newsfeedapp.UPDATE_SAVED_LIST_EVENT
import com.chili.newsfeedapp.data.db.AppDatabase
import com.chili.newsfeedapp.data.db.SavedNewsDataSourceLocal
import com.chili.newsfeedapp.data.db.entity.BaseNewsEntry
import com.chili.newsfeedapp.data.db.entity.NewsEntry
import com.chili.newsfeedapp.data.db.entity.SavedNewsEntry

class DetailsViewModel(var appDatabase: AppDatabase) : ViewModel() {

    var savedNewsDataSourceLocal = SavedNewsDataSourceLocal(appDatabase.savedNewsDao())

    fun getNewsByUrl(url: String): BaseNewsEntry {
        return appDatabase.savedNewsDao().getSavedNewsByUrl(url) ?: appDatabase.newsDao().getNewsByUrl(url)
    }

    private fun insertIntoSaveNewsDb(savedNewsEntry: SavedNewsEntry) {
        appDatabase.savedNewsDao().upsert(savedNewsEntry)
        sendUpdateBroadcast()
    }

    fun deleteFromSavedNewsDb(savedNewsEntry: SavedNewsEntry) {
        appDatabase.savedNewsDao().deleteSavedNews(savedNewsEntry)
        sendUpdateBroadcast()
    }

    fun deleteFromSavedNewsDbByUrl(url: String) {
        appDatabase.savedNewsDao().deleteSavedNewsByUrl(url)
        sendUpdateBroadcast()
    }

    private fun sendUpdateBroadcast() {
        savedNewsDataSourceLocal.getNews()
        LocalBroadcastManager.getInstance(NewsFeedApplication.instance).sendBroadcast(Intent(UPDATE_SAVED_LIST_EVENT))
    }

    fun insertOrDelete(selected: Boolean,newsUrl : String) {
        if (selected) {
            deleteFromSavedNewsDbByUrl(newsUrl)
        } else {
            val newsEntry = getNewsByUrl(newsUrl)
            newsEntry.isSaved = true
            if (isFromNewsFeed(newsEntry))
                insertIntoSaveNewsDb((newsEntry as NewsEntry).toSavedNewsEntry())
            else
                insertIntoSaveNewsDb(newsEntry as SavedNewsEntry)

        }
    }

    private fun isFromNewsFeed(baseNewsEntry: BaseNewsEntry): Boolean {
        return baseNewsEntry is NewsEntry
    }



    class DetailsViewModelFactory(private val appDatabase: AppDatabase) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return DetailsViewModel(appDatabase) as T
        }
    }


}
