package com.chili.newsfeedapp.ui.newsfeed

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chili.newsfeedapp.NEWS_ITEM_URL
import com.chili.newsfeedapp.NewsFeedApplication
import com.chili.newsfeedapp.R
import com.chili.newsfeedapp.Sources
import com.chili.newsfeedapp.adapters.NewsFeedRecyclerAdapter
import com.chili.newsfeedapp.data.db.entity.NewsEntry
import com.chili.newsfeedapp.ui.details.DetailsFragment
import kotlinx.android.synthetic.main.news_feed_fragment.*


class NewsFeedFragment : Fragment(), NewsFeedRecyclerAdapter.ItemClickListener {
    companion object {
        fun newInstance() = NewsFeedFragment()
    }

    private lateinit var viewModel: NewsFeedViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.news_feed_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initNewsFeedRecyclerView()
    }

    private fun initNewsFeedRecyclerView() {
        newsFeedRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            val newsFeedRecyclerAdapter = NewsFeedRecyclerAdapter(listOf())
            newsFeedRecyclerAdapter.setOnclickListener(this@NewsFeedFragment)
            adapter = newsFeedRecyclerAdapter
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =
                ViewModelProviders.of(this, NewsFeedViewModelFactory(NewsFeedApplication.instance.newsFeedRepository))
                        .get(NewsFeedViewModel::class.java)

        viewModel.getNewsFeed(arrayOf(Sources.ABC_NEWS,Sources.ANSA,Sources.FOX_SPORTS)).observe(this, Observer {
            it?.let { newsListUpdated: List<NewsEntry> ->
                if (it.isNotEmpty()) {
                    progress.visibility = View.GONE
                }
                (newsFeedRecyclerView.adapter as NewsFeedRecyclerAdapter).updateList(newsListUpdated)
            }
        })
    }

    override fun onItemClicked(newsEntry: NewsEntry, position: Int) {
        goToDetailScreen(newsEntry.url)

    }

    private fun goToDetailScreen(url: String) {
        val fragment = DetailsFragment.newInstance()
        val bundle = Bundle()
        bundle.putString(NEWS_ITEM_URL, url)
        fragment.arguments = bundle
        activity?.supportFragmentManager?.let {
            it.beginTransaction()
                    .replace(R.id.mainFragmentContainer, fragment)
                    .addToBackStack(null)
                    .commit()
        }


    }


}
