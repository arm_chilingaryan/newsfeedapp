package com.chili.newsfeedapp.ui.newsfeed

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.chili.newsfeedapp.Sources
import com.chili.newsfeedapp.data.db.entity.NewsEntry
import com.chili.newsfeedapp.data.repository.NewsRepository

class NewsFeedViewModel(private val newsRepository: NewsRepository) : ViewModel() {

    fun getNewsFeed(newsSources : Array <Sources>): LiveData<List<NewsEntry>> {
        return newsRepository.getNewsFeed(newsSources)
    }
}


class NewsFeedViewModelFactory(private val newsRepository: NewsRepository) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NewsFeedViewModel(newsRepository) as T
    }
}