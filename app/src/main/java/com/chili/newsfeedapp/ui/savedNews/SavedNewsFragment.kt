package com.chili.newsfeedapp.ui.savedNews

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chili.newsfeedapp.NEWS_ITEM_URL
import com.chili.newsfeedapp.NewsFeedApplication
import com.chili.newsfeedapp.R
import com.chili.newsfeedapp.UPDATE_SAVED_LIST_EVENT
import com.chili.newsfeedapp.adapters.SavedNewsFeedRecyclerAdapter
import com.chili.newsfeedapp.data.db.SavedNewsDataSourceLocal
import com.chili.newsfeedapp.data.db.entity.SavedNewsEntry
import com.chili.newsfeedapp.ui.details.DetailsFragment
import kotlinx.android.synthetic.main.saved_news_fragment.*


class SavedNewsFragment : Fragment(), SavedNewsFeedRecyclerAdapter.ItemClickListener {

    companion object {
        fun newInstance() = SavedNewsFragment()
    }

    private lateinit var viewModel: SavedNewsViewModel

    private val updateBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            viewModel.getSavedNews()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        LocalBroadcastManager.getInstance(NewsFeedApplication.instance)
            .registerReceiver(updateBroadcastReceiver, IntentFilter(UPDATE_SAVED_LIST_EVENT))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.saved_news_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initNewsFeedRecyclerView()
    }

    override fun onDestroy() {
        updateBroadcastReceiver?.let {
            LocalBroadcastManager.getInstance(NewsFeedApplication.instance).unregisterReceiver(it)
        }
        super.onDestroy()

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(
            this,
            SavedNewsViewModelFactory(SavedNewsDataSourceLocal(NewsFeedApplication.instance.appDatabase.savedNewsDao()))
        ).get(SavedNewsViewModel::class.java)

        viewModel.getSavedNews().observe(this, Observer {
            it?.let((savedNewsRecyclerView.adapter as SavedNewsFeedRecyclerAdapter)::updateList)
        })
    }

    private fun initNewsFeedRecyclerView() {
        savedNewsRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            val savedNewsRecyclerAdapter = SavedNewsFeedRecyclerAdapter(listOf())
            savedNewsRecyclerAdapter.setOnclickListener(this@SavedNewsFragment)
            adapter = savedNewsRecyclerAdapter
        }
    }


    override fun onNewsItemClicked(newsEntry: SavedNewsEntry, position: Int) {
        goToDetailScreen(newsEntry.url)

    }

    override fun onDeleteClicked(newsEntry: SavedNewsEntry) {
        viewModel.deleteItem(newsEntry)
    }

    private fun goToDetailScreen(url: String) {
        val fragment = DetailsFragment.newInstance()
        val bundle = Bundle()
        bundle.putString(NEWS_ITEM_URL, url)
        fragment.arguments = bundle
        activity?.supportFragmentManager?.let {
            it.beginTransaction()
                .replace(R.id.mainFragmentContainer, fragment)
                .addToBackStack(null)
                .commit()
        }
    }

}
