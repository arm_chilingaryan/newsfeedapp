package com.chili.newsfeedapp.ui.savedNews

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider
import com.chili.newsfeedapp.data.db.AppDatabase
import com.chili.newsfeedapp.data.db.SavedNewsDataSourceLocal
import com.chili.newsfeedapp.data.db.entity.NewsEntry
import com.chili.newsfeedapp.data.db.entity.SavedNewsEntry
import com.chili.newsfeedapp.data.repository.NewsRepository
import com.chili.newsfeedapp.ui.newsfeed.NewsFeedViewModel

class SavedNewsViewModel(private var savedNewsDataSourceLocal: SavedNewsDataSourceLocal) : ViewModel() {

    fun getSavedNews(): LiveData<List<SavedNewsEntry>> {
        savedNewsDataSourceLocal.getNews()
        return savedNewsDataSourceLocal.dbNews
    }

    fun deleteItem(newsEntry: SavedNewsEntry){
        savedNewsDataSourceLocal.savedNewsDao.deleteSavedNews(newsEntry)
        savedNewsDataSourceLocal.getNews()
    }
}


class SavedNewsViewModelFactory(private val savedNewsDataSourceLocal: SavedNewsDataSourceLocal) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SavedNewsViewModel(savedNewsDataSourceLocal) as T
    }
}